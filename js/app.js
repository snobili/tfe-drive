// Set app language
window.Activity = window.Activity || window.MozActivity;

/*
window.onerror= function(msg,url, line)
{
    alert('JAVASCRIPT ERROR: '+url+'  :'+line+' / '+msg);
};
*/

navigator.appLanguage = localStorage.getItem('lang') || navigator.language;

// Init classes
var translate = navigator.mozL10n.get;


var vibrate = new Vibrate();
var sdcard = new Sdcard();
var settings = new Settings();
var gallery = new Gallery();
var accounts = new Accounts();
var files = new Files();
var internal = new Internal();
var google = new Google();
var dropbox = new Dropbox();
var box = new Box();
var onedrive = new OneDrive();
var webdav = new Webdav();
var ftp = new Ftp();
var online = new Online();
var sync = new Sync();
var notif = new Notif();
var queue = new Queue();


// DOMContentLoaded is fired once the document has been loaded and parsed,
// but without waiting for other external resources to load (css/images/etc)
// That makes the app more responsive and perceived as faster.
// https://developer.mozilla.org/Web/Reference/Events/DOMContentLoaded
window.addEventListener('DOMContentLoaded', function() {
    'use strict';

    // Once locales are loaded, load the app
    navigator.mozL10n.ready(function()
    {
        // Init global
        vibrate.init();
        sdcard.init();
        accounts.init();
        files.init();
        settings.init();
        notif.init();
        queue.init();

        // Init apis
        internal.init();
        google.init();
        dropbox.init();
        box.init();
        onedrive.init();
        webdav.init();
        ftp.init();
        online.init();

        // Welcome!
        var version = document.getElementById('version').innerHTML;
        if(!localStorage.getItem('welcome_'+version))
        {
            localStorage.setItem('welcome_'+version, 1);
            var select = new Selector();

            var div = document.createElement('div');
            var p = document.createElement('p');
            div.appendChild(p);
            p.innerHTML=translate('welcome_text');

            select.create('',version, div,
            [
            {
                'text':  translate('open_changelog'),
                'icon':  'exchange',
                'autoclose': false,
                'callback':  settings.openChangelog.bind(settings)
            },
            {
                'text':  translate('close'),
                'icon':  'close',
                'callback':  function() {}
            }
            ]);
        }
    });
});

function callback(url)
{
    if(/google/.test(url))
    {
        google.callback(url);
    }
    else if(/dropbox/.test(url))
    {
        dropbox.callback(url);
    }
    else if(/boxcom/.test(url))
    {
        box.callback(url);
    }
    else if(/onedrive/.test(url))
    {
        onedrive.callback(url);
    }
}

