var Internal = function(){};
Internal.prototype = new Apiprot();

Internal.prototype.icon='cloud';
Internal.prototype.type='Internal';
Internal.prototype.dbname='Internal';
Internal.prototype.star_available = false;
Internal.prototype.delete_available = false;
Internal.prototype.download_available=false;
//Internal.prototype.move_folder_available=false;
Internal.prototype.upload_available=false;
Internal.prototype.nocache=true;

Internal.prototype.init = function()
{
    var self=this;
    this.accounts = {};

    this.account=
    {
        id: 'internal',
        email: translate('internal_sdcards'),
        type: 'sdcard'
    };

    accounts.add(this.account, this);

    this.initDb();
};

Internal.prototype.setAccount = function(id)
{
    var self=this;
    return new Promise(function(ok, reject)
    {
        ok();
    });
};


Internal.prototype.list_dir = function(id)
{
    var self=this;

    return new Promise(function(ok, reject)
    {
        id=id.replace(self.account.id+'_','');
        if(id=='root')
        {
            id='/';
        }
        sdcard.list_dirs().then(function(dirs)
        {
            var segments = id.split(/\//);
            var found=true;
            segments.forEach(function(segment)
            {
                if(segment)
                {
                    if(dirs.dirs[segment])
                    {
                        dirs = dirs.dirs[segment];
                    }
                    else
                    {
                        console.error('cannot find segment ',segment);
                        return ok({items:[]});
                    }
                }
            });

            var result = [];
            Object.keys(dirs.dirs).forEach(function(dir)
            {
                result.push(
                {
                    parent: self.account.id+'_'+id,
                    icon: 'img/icons/dropbox/folder.gif',
                    mime: '',
                    creation_date: null,
                    modified_date: null,
                    id: self.account.id+'_'+id+dir+'/',
                    realid: id+dir+'/',
                    name: dir,
                    thumbnail: null,
                    size: 0,
                    download: null,
                    is_folder:  true,
                    is_starred:  false,
                    is_hidden:  false,
                    is_trashed:  false
                });
            });

            sdcard.list_files(id,true).then(function(files)
            {
                files.forEach(function(file)
                {
                    var filename = file.name.replace(/^.*\//,'');
                    var mime =filename.indexOf('.')!==-1 ? filename.replace(/^.*\./,'') : '???';
                    var icon =  'page_white';

                    mimeData = self.getMimeAndIcon(filename);
                    mime = mimeData.mime;
                    icon=mimeData.icon;
                    var is_image = /image/.test(mime);

                    result.push({
                        parent: self.account.id+'_'+id,
                        icon: 'img/icons/dropbox/'+(icon)+'.gif',
                        mime: mime,
                        creation_date: null,
                        modified_date: file.lastModifiedDate.getTime(),
                        id: self.account.id+'_'+id+filename,
                        realid: id+'/'+filename,
                        size: file.size,
                        name: filename,
                        thumbnail: is_image ? 'sdcard://'+id+filename+'@400x400' : null,
                        bigthumbnail: is_image ? 'sdcard://'+id+filename : null,
                        download: id+'/'+filename,
                        is_folder:  false,
                        is_starred:  false,
                        is_hidden:  false,
                        is_trashed:  false
                    });
                });
                
                ok({items:result});
            }, function()
            {
                console.error('error list files');
                reject();
            });
        }, reject);
    });
};


Internal.prototype.open = function(received_item)
{
    var self=this;
    return new Promise(function(ok, reject)
    {
        sdcard.getfile(received_item.realid.replace(/^\//,'')).then(function(file)
        {
            file.name=received_item.name;

            if(CustomActivity.isPicking)
            {
                CustomActivity.send(file);
                return ok();
            }

            var activity = new Activity({
                name: 'open',
                data: {
                type: file.type,
                allowSave: false,
                blob: file,
                title: received_item.name
                }
            });
            activity.onsuccess= ok;
            activity.onerror = function() {
                console.error('error ',this.error);
                if(this.error.name==='NO_PROVIDER')
                {
                    files.no_provider(file);
                    ok();
                }
                else
                {
                    console.error('big fail? ',this.error, received_item);
                    reject();
                }
            };
        }, function(err)
        {
            console.error('fail open ',err,this);
            reject();
        });
    });
};

Internal.prototype.rename = function(id, newname, is_folder)
{
    // TODO
    var self=this;
    id=id.replace(self.account.id+'_','');

    return new Promise(function(ok, reject)
    {
        if(!is_folder)
        {
            sdcard.getfile(id).then(function(file)
            {
                dest = id.replace(/[^\/]+\/?$/, newname);
                sdcard.add(file, dest, true).then(
                function()
                {
                    sdcard.delete(id,true).then(ok, reject);
                }, reject);
            }, reject);
        }
        else
        {
            var dest = id.replace(/[^\/]+\/?$/, newname+'/');
            self.move(id, dest, true).then(ok, reject);
        }
    });
};

Internal.prototype.delete = function(id, is_folder)
{
    var self=this;
    id=id.replace(self.account.id+'_','');
    return new Promise(function(ok, reject)
    {
        if(!is_folder)
        {
            sdcard.delete(id,true).then(ok, reject);
        }
        else
        {
            self.list_dir(id).then(function(dirs)
            {
                var promises=[];
                dirs.items.forEach(function(item)
                {
                    promises.push(self.delete(item.id, item.is_folder));
                });
                Promise.all(promises).then(function()
                {
                    self.delete(id).then(ok, reject);
                }, reject);
            });
        }
    });
};

Internal.prototype.share = function(item)
{
    var self=this;
    return new Promise(function(ok, reject)
    {
        sdcard.getfile(item.realid.replace(/^\//,'')).then(function(file)
        {
            return ok(file);
        }, function()
        {
            return reject(translate('error_opening_file'));
        });
    });
};

Internal.prototype.move = function(id, destination, is_folder)
{
    // TODO
    var self=this;
    id=id.replace(self.account.id+'_','');
    destination=destination.replace(self.account.id+'_','');
    if(destination=='root')
    {
        destination='/';
    }

    return new Promise(function(ok, reject)
    {
        if(!is_folder)
        {
            sdcard.getfile(id).then(function(file)
            {
                dest = destination+'/'+id.replace(/.*\//, '');
                sdcard.add(file, dest, true).then(
                function()
                {
                    sdcard.delete(id,true).then(ok, reject);
                }, reject);
            }, reject);
        }
        else
        {
            self.list_dir(id).then(function(dirs)
            {
                var promises=[];
                var prefix = id.replace(/^.*?([^\/]+\/)$/,'$1');
                dirs.items.forEach(function(item)
                {
                    var dest;
                    dest = destination+prefix;

                    promises.push(self.move(item.id, dest, item.is_folder));
                });
                Promise.all(promises).then(function()
                {
                    sdcard.delete(id+'.empty').then(ok, reject);
                }, reject);
            }, reject);
        }
    });
};

Internal.prototype.copy = function(id, destination, is_folder)
{
    var self=this;
    id=id.replace(self.account.id+'_','');
    destination=destination.replace(self.account.id+'_','');
    if(destination=='root')
    {
        destination='/';
    }

    return new Promise(function(ok, reject)
    {
        if(!is_folder)
        {
            var file = sdcard.getfile(id).then(function(file)
            {
                var name = file.name.replace(/^.*\//,'');
                sdcard.add(file,destination+'/'+name,true).then(ok, reject);
            },reject);
        }
    });
};

Internal.prototype.mkdir = function(id, newname)
{
    var self=this;
    id=id.replace(self.account.id+'_','');



    return new Promise(function(ok, reject)
    {
        var dir =
        {
            parent: self.account.id+'_'+id,
            icon: 'img/icons/dropbox/folder.gif',
            mime: '',
            creation_date: null,
            modified_date: null,
            id: self.account.id+'_'+id+newname+'/',
            realid: id+newname+'/',
            name: newname,
            thumbnail: null,
            size: 0,
            download: null,
            is_folder:  true,
            is_starred:  false,
            is_hidden:  false,
            is_trashed:  false
        };

        var blob = new Blob( [ '' ], { type: 'text/plain'} );
        blob.name='/.empty';
        self.create(dir.id+'/.empty', blob).then(function()
        {
            ok(dir);
        }, reject);
    });
}

Internal.prototype.create = function(id,blob)
{
    id=id.replace(this.account.id+'_','');

    return new Promise(function(ok, reject)
        {
            var dest= id+blob.name.replace(/.*\//,'');
            sdcard.add(blob, dest, true).then(ok, reject);
        });
};


Internal.prototype.getInfo = function(account)
{
    return new Promise(function(ok, reject)
    {
        var promises=[];
        sdcard.sdcards.forEach(function(sdcard_sub)
        {
            var newp = new Promise(function(subok, subreject)
            {
                Promise.all([sdcard.getUsedSpace(sdcard_sub), sdcard.getFreeSpace(sdcard_sub)])
                .then(function(values)
                {
                    subok((sdcard_sub.storageName || '')+' '+ files.getReadableFileSizeString(values[0])+' / '+ files.getReadableFileSizeString(values[1]+values[0]));
                }, function(e)
                {
                    console.error('fail stat sdcard ',e);
                    subreject();
                });
            });
            promises.push(newp);
        });

        Promise.all(promises).then(function(values)
        {
            ok(values.join('<br>'));
        }, function()
        {
            console.error('error all promises ',this);
            reject();
        });
    });
};

Internal.prototype.search = function(search)
{
    var self=this;

    return new Promise(function(ok, reject)
    {
        sdcard.search(search).then(function(data)
        {
            var result = [];

            data.dirs.forEach(function(dir)
            {
                id= dir.replace(/^(.*\/)[^\/]+\/$/,'$1');
                dirname= dir.replace(/^(.*\/)?([^\/]+)\/$/,'$2');

                result.push(
                {
                    parent: self.account.id+'_'+id,
                    icon: 'img/icons/dropbox/folder.gif',
                    mime: '',
                    creation_date: null,
                    modified_date: null,
                    id: self.account.id+'_'+dir,
                    realid: dir,
                    name: dirname,
                    thumbnail: null,
                    size: 0,
                    download: null,
                    is_folder:  true,
                    is_starred:  false,
                    is_hidden:  false,
                    is_trashed:  false
                });
            });

            data.files.forEach(function(file)
            {
                var id=file.name;
                var filename = file.name.replace(/^.*\//,'');
                var mime =filename.indexOf('.')!==-1 ? filename.replace(/^.*\./,'') : '???';
                var icon =  'page_white';

                mimeData = self.getMimeAndIcon(filename);
                mime = mimeData.mime;
                icon=mimeData.icon;
                var is_image = /image/.test(mime);

                result.push({
                    parent: self.account.id+'_'+id,
                    icon: 'img/icons/dropbox/'+(icon)+'.gif',
                    mime: mime,
                    creation_date: null,
                    modified_date: file.lastModifiedDate.getTime(),
                    id: self.account.id+'_'+id,
                    realid: id,
                    size: file.size,
                    name: filename,
                    thumbnail: is_image ? 'sdcard://'+id+'@400x400' : null,
                    bigthumbnail: is_image ? 'sdcard://'+id : null,
                    download: id,
                    is_folder:  false,
                    is_starred:  false,
                    is_hidden:  false,
                    is_trashed:  false
                });
            });
            ok({items:result});
        }, function()
        {
            console.error('error calling sdcard.search');
            reject();
        });
    });
};
