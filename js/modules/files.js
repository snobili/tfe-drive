var Files = function()
{
    this.selected={};
};


Files.prototype.silent_init = function()
{
    // fake binds
    this.section_title = document.createElement('div');
    this.files = document.createElement('div');
    this.files_ariane = document.createElement('div');
    this.files_container = document.createElement('div');
    this.alert_container = document.createElement('div');
    this.alert_msg = document.createElement('div');

    this.home_btn = document.createElement('div');
    this.add_btn = document.createElement('div');
    this.more_btn = document.createElement('div');

    return this.initDb();
};

Files.prototype.init = function()
{
    var self=this;
    this.silent_init();
    this.bind();
    this.update_fetched=null;
    this.inited=1;

    /*
    self.refresh_interval=window.setInterval(function()
    {
        self.refresh();
    }, 1000*600 );
    */

    Array.forEach(document.querySelectorAll('.goto_files'), function(item)
    {
        item.addEventListener('click', self.open.bind(self));
    });
    //this.home_btn.addEventListener('click', vibrate.button.bind(vibrate));
    this.add_btn.addEventListener('click', vibrate.button.bind(vibrate));
    this.update_btn.addEventListener('click', vibrate.button.bind(vibrate));
    this.search_btn.addEventListener('click', vibrate.button.bind(vibrate));
    this.sync_btn.addEventListener('click', vibrate.button.bind(vibrate));
    this.more_btn.addEventListener('click', vibrate.button.bind(vibrate));

    //this.home_btn.addEventListener('click', this.home.bind(this));
    this.add_btn.addEventListener('click', this.add.bind(this));
    this.update_btn.addEventListener('click', this.update.bind(this));
    this.search_btn.addEventListener('click', this.search.bind(this,null));
    this.sync_btn.addEventListener('click', this.sync.bind(this));
    this.more_btn.addEventListener('click', this.more.bind(this));
    this.upload_btn.addEventListener('click', this.upload_selected.bind(this));
};



Files.prototype.bind = function()
{
    this.files = document.querySelector('.files');
    this.files_ariane = document.querySelector('.files_ariane');
    this.files_container = document.querySelector('.files_container');

    this.alert_container = document.querySelector('.alert_container');
    this.alert_msg = document.querySelector('.alert');

    this.add_btn = document.querySelector('.add_btn');
    //this.home_btn = document.querySelector('.home_btn');
    this.update_btn = document.querySelector('.update_btn');
    this.search_btn = document.querySelector('.search_btn');
    this.sync_btn = document.querySelector('.sync_btn');
    this.more_btn = document.querySelector('.more_btn');
    this.upload_btn = document.querySelector('.upload_btn');

    this.section_title = this.files.querySelector('.section_title');
};

Files.prototype.initDb = function()
{
    var self=this;
    return new Promise(function(ok, reject)
    {
        var request = indexedDB.open('Files',3.0);
        request.onsuccess = function (e) {
            self.db = e.target.result;
            ok();
        };
        request.onerror = function (e) {
            console.error(e);
            console.error('error open file ',this);
            request = indexedDB.deleteDatabase('Files');
            request.onsuccess= function()
            {
                self.initDb().then(ok, reject);
            };
            request.onerror= function()
            {
                console.error('error init db Files');
                self.alert(translate('error_open_db', 10000));
                reject();
            };
        };

        request.onupgradeneeded = function (e) {
            self.db = e.target.result;

            if (self.db.objectStoreNames.contains("files")) {
                self.db.deleteObjectStore("files");
            }

            var objectStore = self.db.createObjectStore('files', { keyPath: 'parent_id', autoIncrement: true });
            objectStore.createIndex("parent_id", "parent_id", { unique: true });
            objectStore.createIndex("id", "id", { unique: false });
            objectStore.createIndex("parent", "parent", { unique: false });
            objectStore.createIndex("is_folder", "is_folder", { unique: false });

            objectStore = self.db.createObjectStore('sync', { keyPath: 'id', autoIncrement: true });
            objectStore.createIndex("api_id", "api_id", { unique: false });
            objectStore.createIndex("source", "source", { unique: false });
            objectStore.createIndex("destination", "destination", { unique: false });
        };
    });

};

Files.prototype.setAccount = function(id, obj)
{
    this.accountid = id;
    this.api = obj;
    this.update_fetched=null;

    var txt = document.createTextNode(obj.account.email);

    // reset objects
    this.selected={};

    this.openRoot();
    this.open();

    if(this.sync_btn)
    {
        if(this.api.upload_available===false)
        {
            this.sync_btn.classList.add('hidden');
        }
        else
        {
            this.sync_btn.classList.remove('hidden');
        }
    }
};

Files.prototype.upload_selected = function()
{
    var self=this;
    CustomActivity.done_upload();

    if(CustomActivity.activityReq &&
            CustomActivity.activityReq.source &&
            CustomActivity.activityReq.source.data &&
            CustomActivity.activityReq.source.data.blobs)
        {
            var blobs = CustomActivity.activityReq.source.data.blobs;
            var promises = [];
            var destination = this.subdirs[this.subdirs.length-1];
            if(!/special_/.test(destination.id) && !(destination.id===this.accountid+'_/' && this.api.type=='Google'))
            {
                self.alert(translate('uploading_file'),99999999);
                self.upload_btn.classList.add('hidden');
                blobs.forEach(function(blob)
                {
                    queue.add(self.api, blob.name, [ 'create', destination.id, blob]);
                });
            }
            else
            {
                self.alert(translate('invalid_destination_path'));
            }
        }
};

Files.prototype.openRoot = function()
{
    this.cancel_search();
    this.subdirs=[];
    this.buildDir(this.accountid+'_/',this.api.account.email, false);
};

/* Force update of file list */
Files.prototype.update = function()
{
    var self=this;
    this.update_fetched={};
    var prev;

    if(this.subdirs && this.subdirs.length>0)
    {
        if(!online.online && this.api.type!='Internal')
        {
            return this.alert(translate('cannot_perform_action_offline'));
        }
        prev= this.subdirs.pop();
        this.buildDir(prev.id, prev.name , true);
    }
};

Files.prototype.cancel_search = function()
{
    this.searching=false;
    this.search_btn.querySelector('.button_text').innerHTML= translate('menu_search');
    this.search_btn.classList.remove('active');
};

Files.prototype.search = function(search)
{
    var self=this;
    if(this.searching && !search)
    {
        this.openRoot();
        return;
    }
    if(!online.online && this.api.type!='Internal')
    {
        return this.alert(translate('cannot_perform_action_offline'));
    }

    if(!search)
    {
        search = prompt(translate('search_enter'));
    }
    if(!search)
    {
        return;
    }
    this.searching=true;
    this.search_btn.querySelector('.button_text').innerHTML= translate('cancel_search');
    this.search_btn.classList.add('active');


    this.subdirs=[];
    this.current_id='special_search';
    this.current_name=search;
    this.update_status();

    this.files_ariane.innerHTML= '';
    text = document.createTextNode(translate('menu_search')+' : '+search);
    this.files_ariane.appendChild(text);

    this.api.search(search).then(function(data)
    {
        self.buildDir_receive('special_search', 'special_search', data);
    }, function()
    {
    });
};

Files.prototype.edit = function(item, e)
{
    if(e)
    {
        if(e.preventDefault) { e.preventDefault(); }
        if(e.stopPropagation) { e.stopPropagation(); }
    }

    if(!item)
    {
        // No edition for root directory
        if(this.subdirs.length<=1)
        {
            return;
        }

        var last = this.subdirs[files.subdirs.length-1];
        item=last;
    }

    var selector = new Selector();
    var items=[];


    if(!item.is_trashed)
    {
        if(!item.is_folder)
        {
            items.push({
                'text':  CustomActivity.isPicking ? translate('select_file') : translate('item_open'),
                'icon':  'arrow-right',
                'autoclose': false,
                'callback':  this.open_file.bind(this, item)
            });
            if(!CustomActivity.isPicking)
            {
                items.push({
                    'text':  translate('item_share'),
                    'icon':  'share-alt',
                    'callback':  this.share.bind(this, [item])
                });
            }
        }
        if(!CustomActivity.isPicking)
        {
            if(this.api.download_available!==false)
            {
                items.push({
                    'text':  translate('item_download'),
                    'icon':  'download',
                    'callback':  this.download.bind(this, item)
                });
            }
            items.push({
                'text':  translate('item_rename'),
                'icon':  'font',
                'callback':  this.rename.bind(this, item.id ,item.name, item.is_folder)
            });
            if(this.api.move_folder_available!==false)
            {
                items.push({
                    'text':  translate('item_cut'),
                    'icon':  'cut',
                    'callback':  this.cut.bind(this, item)
                });
            }
            if(!item.is_folder)
            {
                items.push({
                    'text':  translate('item_copy'),
                    'icon':  'copy',
                    'callback':  this.copy.bind(this, item)
                });
            }

            items.push({
                'text':  translate('item_trash'),
                'icon':  'trash',
                'callback':  this.delete.bind(this, item.id ,item.is_folder)
            });

            if(this.api.star_available)
            {
                items.push({
                    'text':  item.is_starred ? translate('item_unstar') : translate('item_star'),
                    'icon':  item.is_starred ? 'star' : 'star-o',
                    'callback':  this.star.bind(this, item)
                });
            }
        }
    }
    // Trashed items
    else
    {
        items.push({
            'text':  translate('item_recover'),
            'icon':  'check',
            'callback':  this.recover.bind(this, item)
        });
        items.push({
            'text':  translate('item_delete'),
            'icon':  'remove',
            'callback':  this.realdelete.bind(this, item.id ,item.is_folder)
        });
    }

    var creation_date = item.creation_date ? new Date(item.creation_date).toLocaleFormat(translate("fulldate_format")) : null;
    var modification_date = new Date(item.modified_date).toLocaleFormat(translate("fulldate_format"));
    var description=
            '<dl>'+
            '<dt>'+translate('mime_type')+'</dt>'+
            '<dd>'+(item.is_folder ? translate('folder') : item.mime)+'</dd>'+
            '</dl>'+

            (!item.is_folder ? 
            '<dl>'+
            '<dt>'+translate('filesize')+'</dt>'+
            '<dd>'+this.getReadableFileSizeString(item.size)+'</dd>'+
            '</dl>' : '')+

            (creation_date ?
            '<dl>'+
            '<dt>'+translate('creation_date')+'</dt>'+
            '<dd>'+creation_date+'</dd>'+
            '</dl>' : '')+

            '<dl>'+
            '<dt>'+translate('modification_date')+'</dt>'+
            '<dd>'+modification_date+'</dd>'+
            '</dl>';

        if(item.thumbnail && settings.getShowThumbnails())
        {
            description+='<p id="thumb_preview"></p>';
            sdcard.cache_image(item.realid, item.thumbnail)
            .then((function(content)
            {
                document.getElementById('thumb_preview').style.backgroundImage='url('+(content)+')';
            }).bind());
        }
    this.edit_selector = selector.create(
            CustomActivity.isPicking ? '': 'selector_menu',
            item.name,
            description,
            items);
    return false;
};

Files.prototype.add = function()
{
    var last = this.subdirs[files.subdirs.length-1];

    if(!online.online && this.api.type!='Internal')
    {
        return this.alert(translate('cannot_perform_action_offline'));
    }
    if(/special_/.test(last.id) || (last.id===this.accountid+'_/' && this.api.type=='Google'))
    {
        return;
    }
    if(this.api.upload_available===false)
    {
        return this.new_folder(last.id ,last.name);
    }
    var selector = new Selector();
    var items=[];

    items.push({
        'text':  translate('upload_file'),
        'icon':  'upload',
        'callback':  this.upload_file.bind(this, last.id ,last.name)
    });

    items.push({
        'text':  translate('upload_directory'),
        'icon':  'upload',
        'callback':  this.upload_dir.bind(this, last.id ,last.name)
    });

    items.push({
        'text':  translate('new_folder'),
        'icon':  'folder',
        'callback':  this.new_folder.bind(this, last.id ,last.name)
    });

    selector.create(
            'selector_picker',
            last.name,
            translate('add_item_description'),
            items);
};

Files.prototype.home = function()
{
    if(this.subdirs.length!==1)
    {
        this.openRoot();
    }
    return false;
};

Files.prototype.more = function()
{
    var selector = new Selector();
    var items=[];

    items.push({
        'text':  translate('item_selectall'),
        'icon':  'check-square-o',
        'callback':  this.selectall.bind(this)
    });
    if(Object.keys(this.selected).length>0)
    {
        items.push({
            'text':  translate('item_unselectall'),
            'icon':  'square-o',
            'callback':  this.unselectall.bind(this)
        });

        if(this.api.download_available!==false)
        {
            items.push({
                'text':  translate('item_download'),
                'icon':  'download',
                'callback':  this.download.bind(this, this.selected)
            });
        }
        items.push({
            'text':  translate('item_cut'),
            'icon':  'cut',
            'callback':  this.cut.bind(this, this.selected)
        });
        items.push({
            'text':  translate('item_copy'),
            'icon':  'copy',
            'callback':  this.copy.bind(this, this.selected)
        });
        items.push({
            'text':  translate('item_share'),
            'icon':  'share-alt',
            'callback':  this.share.bind(this, this.selected, false)
        });

        items.push({
            'text':  translate('item_trash'),
            'icon':  'trash',
            'callback':  this.delete_multiple.bind(this, this.selected, false)
        });
    }

    var num = this.files.querySelectorAll('.item_check.fa-check-square-o').length;

    var description= '';
    selector.create(
            'selector_menu',
            translate('multi_selection_title'),
            translate('multi_selection_description', { num: num}),
            items);
    return false;
};


Files.prototype.sync = function()
{
    var self=this;
    var selector = new Selector();
    var items=[];

    var transaction = this.db.transaction([ 'sync' ], 'readwrite');
    var sync_db = transaction.objectStore('sync');
    var sync_index = sync_db.index('api_id');

    sync_index.get(this.accountid);
    var rangeTest = IDBKeyRange.only(this.accountid);

    var sync_items=[];
    sync_index.openCursor(rangeTest).onsuccess = function(event)
    {
        var cursor = event.target.result;
        if (cursor) {
            sync_items.push(cursor.value);
            cursor.continue();
        }
        else
        {
            items.push({
                'text':  translate('sync_new'),
                'icon':  'plus',
                'callback':  self.sync_new.bind(self)
            });

            var description= document.createElement('div');

            var p = document.createElement('p');
            p.innerHTML = translate('sync_intro');
            description.appendChild(p);

            sync_items.forEach(function(sync_item)
            {
                var div =document.createElement('div');
                div.classList.add('sync_item');

                var dl =document.createElement('dl');
                div.appendChild(dl);

                dt = document.createElement('dt');
                dt.innerHTML=translate('sync_type')+':';
                dl.appendChild(dt);
                dd = document.createElement('dd');
                txt = document.createTextNode(sync_item.type == 'upload' ? translate('sync_local_to_remote') : translate('sync_remote_to_local'));
                dd.appendChild(txt);
                dl.appendChild(dd);

                dl =document.createElement('dl');
                div.appendChild(dl);

                dt = document.createElement('dt');
                dt.innerHTML=translate('sync_source')+':';
                dl.appendChild(dt);
                dd = document.createElement('dd');
                var txt = document.createTextNode('/'+sync_item.source_path);
                dd.appendChild(txt);
                dl.appendChild(dd);

                dl =document.createElement('dl');
                div.appendChild(dl);

                dt = document.createElement('dt');
                dt.innerHTML=translate('sync_destination')+':';
                dl.appendChild(dt);
                dd = document.createElement('dd');
                txt = document.createTextNode('/'+sync_item.destination_path);
                dd.appendChild(txt);
                dl.appendChild(dd);

                dl =document.createElement('dl');
                div.appendChild(dl);

                dt = document.createElement('dt');
                dt.innerHTML=translate('sync_last_run')+':';
                dl.appendChild(dt);
                dd = document.createElement('dd');
                txt = document.createTextNode(sync_item.last_update ? new Date(sync_item.last_update).toLocaleFormat(translate("fulldate_format")) : translate('sync_never_runned'));
                dd.appendChild(txt);
                dl.appendChild(dd);

                dl =document.createElement('dl');
                div.appendChild(dl);

                dt = document.createElement('dt');
                dt.innerHTML=translate('sync_last_status')+':';
                dl.appendChild(dt);
                dd = document.createElement('dd');
                txt = document.createTextNode(sync_item.last_update ? (sync_item.last_status ? translate('sync_status_ok') : translate('sync_status_ko')) : '-');
                dd.appendChild(txt);
                dl.appendChild(dd);

                p = document.createElement('p');
                p.className='bb-button bb-delete';
                p.innerHTML=translate('item_delete');
                p.addEventListener('click', vibrate.button.bind(vibrate));
                p.addEventListener('click', selector.close.bind(selector));
                p.addEventListener('click', self.delete_sync.bind(self, sync_item.id, true));
                div.appendChild(p);

                p = document.createElement('p');
                p.className='bb-button bb-recommend';
                p.innerHTML=translate('sync_run');
                p.addEventListener('click', vibrate.button.bind(vibrate));
                p.addEventListener('click', self.manual_run_sync.bind(self, sync_item.id, selector));
                div.appendChild(p);



                description.appendChild(div);
            });


            selector.create(
                    'sync_menu',
                    translate('sync_title'),
                    description,
                    items);
            return false;
        }
    };

};

Files.prototype.manual_run_sync = function(id, selector)
{
    var self=this;
    self.alert(translate('running_task'));
    this.run_sync(id, true).then(function()
    {
        self.alert(translate('task_done'));
        selector.close();
        self.sync();

    }, function()
    {
        self.alert(translate('task_error'));
        selector.close();
        self.sync();
    });
};

Files.prototype.run_sync = function(id, need_confirm)
{
    var self=this;
    return new Promise(function(ok, reject)
    {
        if(!online.online)
        {
            self.alert(translate('cannot_perform_action_offline'));
            ok();
        }
        if(need_confirm && !confirm(translate('sync_confirm_run')))
        {
            return reject();
        }
        sdcard.update();

        var objectStore = self.db.transaction(["sync"], "readwrite").objectStore("sync");
        var request = objectStore.get(id);

        request.onsuccess = function()
        {
            var data = request.result;
            data.last_update = (new Date()).getTime();
            if(data.type=='upload')
            {
                // Get list of sdcard dirs and call with correct source
                sdcard.list_dirs().then(function(dirs)
                {
                    var segments = data.source.split(/\//);
                    var check=true;
                    while(segments.length>0 && check)
                    {
                        var sub = segments.shift();
                        if(sub)
                        {
                            if(!dirs.dirs[sub])
                            {
                                check=false;
                            }
                            else
                            {
                                dirs= dirs.dirs[sub];
                            }
                        }
                    }
                    if(check)
                    {
                        self.upload_picked(data.destination, dirs).then(function()
                        {
                            data.last_status=true;
                            var objectStore = self.db.transaction(["sync"], "readwrite").objectStore("sync");
                            var request = objectStore.put(data);
                            request.onsuccess = function()
                            {
                                ok();
                            };
                            request.onerror = function(err)
                            {
                                reject();
                            };
                            self.alert(translate('task_done'));
                        }, function()
                        {
                            data.last_status=false;
                            var objectStore = self.db.transaction(["sync"], "readwrite").objectStore("sync");
                            var request = objectStore.put(data);
                            request.onsuccess = function()
                            {
                                reject();
                            };
                            request.onerror = function(err)
                            {
                                reject();
                            };
                            self.alert(translate('task_error'));
                        });
                    }
                    else
                    {
                        data.last_status=false;
                        var objectStore = self.db.transaction(["sync"], "readwrite").objectStore("sync");
                        var request = objectStore.put(data);
                        request.onsuccess = function()
                        {
                            reject();
                        };
                        request.onerror = function(err)
                        {
                            reject();
                        };
                        console.error('source not found!');
                        self.alert(translate('task_error'));
                    }
                }, function()
                {
                    data.last_status=false;
                    var objectStore = self.db.transaction(["sync"], "readwrite").objectStore("sync");
                    var request = objectStore.put(data);
                    request.onsuccess = function()
                    {
                        reject();
                    };
                    request.onerror = function(err)
                    {
                        reject();
                    };
                    console.error('source not found!');
                    self.alert(translate('task_error'));
                });
            }
            // download sync
            else
            {
                self.api.file_info(data.source).then(function(source)
                {
                    self.download_picked('/', source, { path: data.destination, dirs: [] }).then(function()
                    {
                        data.last_status=true;

                        var objectStore = self.db.transaction(["sync"], "readwrite").objectStore("sync");
                        var request = objectStore.put(data);
                        request.onsuccess = function()
                        {
                            ok();
                        };
                        request.onerror = function(err)
                        {
                            reject();
                        };
                        self.alert(translate('task_done'));
                    }, function()
                    {
                        data.last_status=false;

                        var objectStore = self.db.transaction(["sync"], "readwrite").objectStore("sync");
                        var request = objectStore.put(data);
                        request.onsuccess = function()
                        {
                            reject();
                        };
                        request.onerror = function(err)
                        {
                            reject();
                        };
                        self.alert(translate('task_error'));
                    });
                }, function()
                {
                    data.last_status=false;

                    var objectStore = self.db.transaction(["sync"], "readwrite").objectStore("sync");
                    request = objectStore.put(data);
                    request.onsuccess = function()
                    {
                        reject();
                    };
                    request.onerror = function(err)
                    {
                        reject();
                    };
                    self.alert('error get file info');
                });
            }
        };
        request.onerror = function()
        {
            reject();
        };
    });
};

Files.prototype.delete_sync = function(id , need_confirm)
{
    var self=this;
    if(need_confirm && !confirm(translate('sync_confirm_del')))
    {
        return false;
    }

    var request = self.db.transaction(["sync"], "readwrite")
        .objectStore("sync")
        .delete(id);
    request.onsuccess = function()
    {
        if(need_confirm) 
        {
            self.sync();
        }
    };
    request.onerror = function()
    {
        if(need_confirm)
        {
            self.sync();
        }
    };

};

Files.prototype.sync_new = function()
{
    var items=[];

    items.push({

        'text':  translate('sync_local_to_remote'),
        'icon':  'cloud-upload',
        'callback':  this.sync_new2.bind(this,'upload')
    });
    items.push({

        'text':  translate('sync_remote_to_local'),
        'icon':  'cloud-download',
        'callback':  this.sync_new2.bind(this,'download')
    });

    var selector = new Selector();
    selector.create(
            '',
            translate('sync_direction'),
            translate('sync_direction_text'),
            items
    );
};

Files.prototype.sync_new2 = function(type)
{
    var self=this;
    if(type=='download')
    {
        this.api.list_dirs('root').then(function(dirs)
        {
            self.alert_hide();
            self.local_dirs=dirs;

            var selector = new Selector();

            selector.create_picker(
                    {
                        title: translate('sync_title'),
                        extra: translate('sync_select_source'),
                        root: dirs,
                        dir_callback: self.api._subdir_build.bind(self.api),
                        callback: self.sync_new3.bind(self,type)
                    }
            );
        }, function()
        {
            self.alert(translate('error_loading_dir_structure'));
        });
    }
    else
    {
        sdcard.list_dirs().then(function(dirs)
        {
            self.alert_hide();
            self.local_dirs=dirs;

            var selector = new Selector();

            selector.create_picker(
                    {
                        title: translate('sync_title'),
                        extra: translate('sync_select_source'),
                        root: dirs,
                        refresh_callback: self.sync_new2.bind(self, type),
                        dir_callback:null,
                        callback: self.sync_new3.bind(self,type)
                    }
            );
        }, function()
        {
            self.alert(translate('error_loading_dir_structure'));
        });
    }
};

Files.prototype.sync_new3 = function(type, source)
{
    var self=this;
    if(type=='upload')
    {
        this.api.list_dirs('root').then(function(dirs)
        {
            self.alert_hide();
            self.local_dirs=dirs;

            var selector = new Selector();

            selector.create_picker(
                    {
                        title: translate('sync_title'),
                        extra: translate('sync_select_destination'),
                        root: dirs,
                        dir_callback: self.api._subdir_build.bind(self.api),
                        callback: self.sync_new4.bind(self,type, source)
                    }
            );
        }, function()
        {
            self.alert(translate('error_loading_dir_structure'));
        });
    }
    else
    {
        sdcard.list_dirs().then(function(dirs)
        {
            self.alert_hide();
            self.local_dirs=dirs;

            var selector = new Selector();

            selector.create_picker(
                    {
                        title: translate('sync_title'),
                        extra: translate('sync_select_destination'),
                        root: dirs,
                        refresh_callback: self.sync_new3.bind(self, type, source),
                        dir_callback:null,
                        callback: self.sync_new4.bind(self,type, source)
                    }
            );
        }, function()
        {
            self.alert(translate('error_loading_dir_structure'));
        });
    }
};

Files.prototype.sync_new4 = function(type, source, destination)
{
    var self=this;

    var transaction = this.db.transaction([ 'sync' ], 'readwrite');
    var sync_db = transaction.objectStore('sync');

    var add = {
        api_id: this.accountid,
        api_type:  this.api.type,
        type: type,
        source: source.id || source.path,
        source_path: source.path,
        destination: destination.id || destination.path,
        destination_path: destination.path
    };
    var request = sync_db.add(add);
    request.onsuccess= function(e)
    {
        self.sync();
    };
    request.onerror= function()
    {
        self.alert(translate('sync_error_adding'), 7000);
        self.sync();
    };
};

Files.prototype.rename = function(id ,name, is_folder)
{
    var self=this;
    if(!online.online && this.api.type!='Internal')
    {
        return this.alert(translate('cannot_perform_action_offline'));
    }
    var newname = prompt(translate('entre_new_name'),name);

    var li = document.querySelector('.files_container li[data-id="'+id.replace(/"/g,'\\"')+'"]');
    if(newname)
    {
        li.classList.add('updating');
        this.api.rename(id, newname, is_folder).then(function()
        {
            self.update();
        }, function()
        {
            self.alert(translate('error_renaming'));
           li.classList.remove('updating');
        });
    }
};


Files.prototype.open_file = function(item)
{
    var self=this;
    
    if(/^image/.test(item.mime) && (item.bigthumbnail||item.thumbnail) && !CustomActivity.isPicking)
    {
        gallery.open_gallery(item);
    }
    else
    {
        this.alert(translate('opening_file'), 99999999);
        this.api.open(item).then(function()
        {
            self.alert_hide();
        }, function(err)
        {
            self.alert(err || translate('open_error'), 7000);
        });
    }
};

Files.prototype.cut = function(item)
{
    var self=this;
    if(!online.online && this.api.type!='Internal')
    {
        return this.alert(translate('cannot_perform_action_offline'));
    }

    this.alert(translate('loading_dir_structure'), 99999999);
    this.api.list_dirs('root').then(function(dirs)
    {
        self.alert_hide();
        self.local_dirs=dirs;

        var selector = new Selector();
        var items=[];

        selector.create_picker(
                {
                    title: translate('select_destination'),
                    root: dirs,
                    dir_callback: self.api._subdir_build.bind(self.api),
                    callback: self.cut_picked.bind(self,item)
                }
        );
    }, function()
    {
        self.alert(translate('error_loading_dir_structure'));
    });
};

Files.prototype.star = function(item)
{
    var self=this;
    if(!online.online && this.api.type!='Internal')
    {
        return this.alert(translate('cannot_perform_action_offline'));
    }

    this.api.star(item.id, !item.is_starred).then(function()
    {
        self.update();
    }, function()
    {
        self.alert(translate('error_star'));
    });
};

Files.prototype.recover = function(item)
{
    var self=this;
    if(!online.online && this.api.type!='Internal')
    {
        return this.alert(translate('cannot_perform_action_offline'));
    }
    if(!confirm(translate('confirm_recover')))
    {
        return false;
    }

    this.api.recover(item).then(function()
    {
        self.clearCache(item.real_parent);
        self.update();
    }, function()
    {
        self.alert(translate('error_recover'));
    });
};

Files.prototype.clearAllCache= function()
{
    var self=this;
    this.alert(translate('clearing_cache'),99999);
    return new Promise(function(ok, reject)
    {
        var promises = [];
        promises.push(self.clearAllCacheDb());
        promises.push(sdcard.clear_cache_image());

        Promise.all(promises).then(function()
                {
                    self.alert(translate('ok_clear'));
                    ok();
                }, function()
                {
                    self.alert(translate('error_clear'));
                    reject();
                });
    });
};

Files.prototype.clearAllCacheDb = function()
{
    var self=this;

    return new Promise(function(ok, reject)
    {
        var transaction = self.db.transaction([ 'files' ] , 'readwrite');
        var dbfiles = transaction.objectStore('files');
        try
        {
            var request = dbfiles.clear();
            request.onerror = function()
            {
                reject();
            };
            request.onsuccess = function()
            {
                ok();
            };
        }
        catch(err)
        {
            console.error('fail ',err,this);
            reject();
        }
    });
};

Files.prototype.realdelete = function(id, is_folder)
{
    var self=this;
    if(!confirm(translate('confirm_realdelete')))
    {
        return false;
    }

    this.api.realdelete(id, is_folder).then(function()
    {
        accounts.updateInfo(self.api, self.api.account);
        self.update();
    }, function()
    {
        self.alert(translate('error_deleting'));
    });
};


Files.prototype.cut_picked = function(item, destination)
{
    var self=this;
    var next=null;

    if(!item.id)
    {
        next=clone(item);
        var keys =Object.keys(item);
        if(keys.length===0)
        {
            self.update();
            return;
        }
        item = item[keys[0]];
        delete next[keys[0]];
        if(keys.length===1)
        {
            self.clearCache(destination.id);
            self.clearCache(item.parent);
        }
    }

    self.alert(translate('moving_file'), 99999999, item.name);
    self.api.move(item.id, destination.id, item.is_folder).then(function()
    {
        self.alert(translate('move_end'));
        if(next)
        {
            self.cut_picked.call(self, next, destination);
        }
        else
        {
            self.clearCache(destination.id);
            self.clearCache(item.parent);
            self.update();
        }

    }, function()
    {
        self.alert(translate('move_error'));
    });
};

Files.prototype.copy = function(item)
{
    var self=this;
    if(!online.online && this.api.type!='Internal')
    {
        return this.alert(translate('cannot_perform_action_offline'));
    }

    this.alert(translate('loading_dir_structure'), 99999999);
    this.api.list_dirs('root').then(function(dirs)
    {
        self.alert_hide();
        self.local_dirs=dirs;

        var selector = new Selector();

        selector.create_picker(
                {
                    title: item.name,
                    root: dirs,
                    dir_callback: self.api._subdir_build.bind(self.api),
                    callback: self.copy_picked.bind(self,item)
                }
        );
    }, function()
    {
        self.alert(translate('error_loading_dir_structure'));
    });
};


Files.prototype.copy_picked = function(item, destination)
{
    var self=this;
    var next=null;

    if(!item.id)
    {
        next=clone(item);
        var keys =Object.keys(item);
        if(keys.length===0)
        {
            self.clearCache(destination.id);
            self.update();
            return;
        }
        item = item[keys[0]];
        delete next[keys[0]];
    }

    self.alert(translate('copying_file'), 99999999,item.name);
    self.api.copy(item.id, destination.id, item.is_folder).then(function()
    {
        self.alert(translate('copy_end'));
        if(next)
        {
            self.copy_picked.call(self, next, destination);
        }
        else
        {
            self.clearCache(destination.id);
            self.update();
        }
    }, function()
    {
        self.alert(translate('copy_error'));
    });
};


Files.prototype.download = function(item)
{
    var self=this;
    if(!online.online && this.api.type!='Internal')
    {
        return this.alert(translate('cannot_perform_action_offline'));
    }

    this.alert(translate('loading_dir_structure'), 99999999);
    sdcard.list_dirs().then(function(dirs)
    {
        self.alert_hide();
        self.local_dirs=dirs;

        var selector = new Selector();
        var items=[];

        selector.create_picker(
                {
                    title: item.name,
                    root: dirs,
                    refresh_callback: self.download.bind(self, item),
                    dir_callback:null,
                    callback: self.download_picked.bind(self,'/',item)
                }
        );
    }, function()
    {
        self.alert(translate('error_loading_dir_structure'));
    });
};

Files.prototype.download_picked = function(prefix, item, destination, numitems)
{
    var self=this;
    var next=null;

    return new Promise(function(ok, reject)
    {
        if(!item.id)
        {
            var next=clone(item);
            var keys =Object.keys(item);
            if(keys.length===0)
            {
                self.update();
                if(numitems===undefined) { files.alert(translate('already_downloaded')); }
                ok(numitems);
                return;
            }
            item = item[keys[0]];
            delete next[keys[0]];
            if(keys.length===1)
            {
                //self.clearCache(destination.id);
                self.clearCache(item.parent);
            }
        }


        self.alert(translate('downloading_file', {file: item.name}), null);
        if(item.is_folder)
        {
            self.api.list_dir(item.id)
                .then(function(dirs)
                {
                    var dest = destination.path+item.name+'/';
                    // remove local files already downloaded
                    sdcard.list_files(dest).then(function(files_local)
                    {
                        files_local = files_local.map(function(x) { return x.replace(dest,''); });
                        var to_download = dirs.items.filter(function(x)
                        {
                            return files_local.indexOf(x.name)===-1;
                        });
                        if(to_download.length>0)
                        {
                            self.download_picked(prefix+item.name+'/', to_download, { path: dest, dirs: [] }, numitems||0)
                            .then(function(numitems)
                                {
                                    if(next)
                                    {
                                        self.download_picked.call(self, prefix, next, destination, numitems||0).then(function(numitems)
                                        {
                                            if(numitems===undefined) { files.alert(translate('already_downloaded')); }
                                            ok(numitems);
                                        },reject);
                                    }
                                    else
                                    {
                                        if(numitems===undefined) { files.alert(translate('already_downloaded')); }
                                        ok(numitems);
                                    }
                                }, reject);
                        }
                        else
                        {
                            if(next)
                            {
                                self.download_picked.call(self, prefix, next, destination,numitems||0).then(function()
                                {
                                    if(numitems===undefined) { files.alert(translate('already_downloaded')); }
                                    ok(numitems);
                                },reject);
                            }
                            else
                            {
                                if(numitems===undefined) { files.alert(translate('already_downloaded')); }
                                ok(numitems);
                            }
                        }
                    },
                    // cannot list sdcard files, download all this directory
                    function()
                    {
                        self.download_picked(prefix+item.name+'/', dirs.items, { path: dest, dirs: [] },numitems||0)
                        .then(function(numitems)
                            {
                                if(next)
                                {
                                    self.download_picked.call(self, prefix, next, destination,numitems||0).then(function(numitems)
                                    {
                                        if(numitems===undefined) { files.alert(translate('already_downloaded')); }
                                        ok(numitems);
                                    },reject);
                                }
                                else
                                {
                                    if(numitems===undefined) { files.alert(translate('already_downloaded')); }
                                    ok(numitems);
                                }
                            }, reject);
                    });
                }, function()
                {
                    self.alert(translate('download_error'), 500, item.name);
                    reject();
                });
        }
        else
        {
            numitems++;
            queue.add(self.api, item.name, [ 'download', item, destination.path]);

            if(next)
            {
                self.download_picked.call(self, prefix, next, destination,numitems||0).then(function(numitems)
                {
                    if(numitems===undefined) { files.alert(translate('already_downloaded')); }
                    ok(numitems);
                },reject);
            }
            else
            {
                if(numitems===undefined) { files.alert(translate('already_downloaded')); }
                ok(numitems);
            }
        }
    });
};

Files.prototype.share = function(items)
{
    var self=this;
    if(!online.online && this.api.type!='Internal')
    {
        return this.alert(translate('cannot_perform_action_offline'));
    }

    var blobs=[];
    var blobs_names=[];
    var promises=[];
    var names =[];
    for(var key  in items)
    {
        names.push(items[key].name);
    }
    this.alert(translate('downloading_file', {file: names.join(', ')}), 99999999);

    for(var key  in items)
    {
        promises.push(this.api.share(items[key]).then(function(blob)
        {
            blob.name = items[key].name;
            blobs_names.push(items[key].name);
            blobs.push(blob);
        }));
    }
    Promise.all(promises).then(function()
    {
        self.alert_hide();
        var activity = new Activity({
            name: 'share',
            data: {
            'type': 'image/*',
            'number': 1,
            'blobs': blobs,
            'filenames': blobs_names,
            'filepaths': blobs_names
            }
        });
    }, function()
    {
        self.alert(translate('download_error'));
    });
};

Files.prototype.delete_multiple = function(items, skip_confirm)
{
    var self=this;
    var keys = Object.keys(items);
    if(skip_confirm || confirm(translate('confirm_delete')))
    {
        if(keys.length>0)
        {
            var key=keys[0];
            var item=items[key];
            delete items[key];
            this.api.delete(item.id, item.is_folder).then(function()
            {
                var request = self.db.transaction(["files"], "readwrite")
                    .objectStore("files")
                    .delete(item.id);
                var li = document.querySelector('.files_container li[data-id="'+item.id.replace(/"/g,'\\"')+'"]');
                if(li)
                {
                    li.parentNode.removeChild(li);
                }
                self.delete_multiple(items, 1);
            });
        }
    }
};

Files.prototype.delete = function(id ,is_folder)
{
    var self=this;
    if(!online.online && this.api.type!='Internal')
    {
        return this.alert(translate('cannot_perform_action_offline'));
    }

    var li = document.querySelector('.files_container li[data-id="'+id.replace(/"/g,'\\"')+'"]');
    if(confirm(translate('confirm_delete')))
    {
        li.classList.add('updating');
        this.api.delete(id, is_folder).then(function()
        {
            accounts.updateInfo(self.api, self.api.account);
            // Remove from db
            var request = self.db.transaction(["files"], "readwrite")
                .objectStore("files")
                .delete(id);

           self.update();
        }, function()
        {
            self.alert(translate('error_deleting'));
           li.classList.remove('updating');
        });
    }
};


Files.prototype.upload_file = function(id, name)
{
    var self=this;

    // Use the 'pick' activity to acquire an image
    var pick = new MozActivity({
        name: "pick",
        data: {
            type: ["image/jpeg", "image/png", "image/jpg"]
        }
    });
    pick.onsuccess = function () {
        self.alert(translate('uploading_file'),99999999);
        var blob = this.result.blob;

        queue.add(self.api, blob.name, [ 'create', id, blob]);
    };
};

Files.prototype.upload_dir = function(id, name)
{
    var self=this;

    this.alert(translate('loading_dir_structure'), 99999999);
    sdcard.list_dirs().then(function(dirs)
    {
        self.alert_hide();
        self.local_dirs=dirs;

        var selector = new Selector();

        selector.create_picker(
                {
                    title: translate('upload_directory'),
                    extra: translate('upload_dir_text'),
                    root: dirs,
                    refresh_callback: self.upload_dir.bind(self, id,name),
                    dir_callback:null,
                    callback: self.upload_picked.bind(self,id)
                }
        );
    }, function()
    {
        self.alert(translate('error_loading_dir_structure'));
    });
};

Files.prototype.upload_picked = function(destination, source)
{
    var self=this;
    return new Promise(function(ok, reject)
    {
        self.alert(translate('loading_dir_structure'), 99999999);

        self.api.list_dir(destination).then(function(dirs)
        {

            var api_dirs = dirs.items.filter(function(x) { return x.is_folder; });
            // upload sub directories here
            for(var source_dir in source.dirs)
            {
                var subdir = source.dirs[source_dir];
                var check = api_dirs.filter(function(x) { return x.name == source_dir; });
                if(check.length===0)
                {
                    self.api.mkdir(destination, source_dir).then(function(newdir)
                    {
                        self.upload_picked(newdir.id, subdir);
                    }, function()
                    {
                        self.alert(translate('error_mkdir'), 7000, source_dir);
                    });
                }
                else
                {
                    self.upload_picked(check[0].id, subdir);
                }
            }

            // upload of files of this directory
            sdcard.list_files(source.path).then(
                function(files)
                {
                    self.upload_dir_each(source.path, destination, dirs.items,files).then(ok,reject);
                },
                function()
                {
                    self.alert(translate('error_loading_dir_structure'), 5000);
                    reject();
                });
        },function()
        {
            self.alert(translate('error_loading_dir_structure'), 5000);
            reject();
        });
    });
};

Files.prototype.upload_dir_each = function(source,destination,api_files,files, total)
{
    var self=this;
    return new Promise(function(ok, reject)
    {
        if(!total)
        {
            total=files.length;
        }
        if(files.length===0)
        {
            return ok();
        }
        var fullfile = files.pop();
        var filename = fullfile.replace(/.*\//,'');
        var check = api_files.filter(function(x)
        {
            return x.name === filename && !x.is_trashed;
        });

        self.alert(translate('loading_dir_structure'), 9999999999,(total-files.length)+'/'+total+' - '+filename);
        // if file missing on the api destination
        if(check.length===0)
        {
            var re = new RegExp('^\/?'+source);
            var file = fullfile.replace(re,'');
            var blob = sdcard.getfile(fullfile).then(function(blob)
            {
                queue.add(self.api, blob.name, [ 'create', destination, blob]);
                if(files.length>0)
                {
                    self.upload_dir_each(source,destination,api_files,files, total).then(ok,reject);
                }
                else
                {
                    ok();
                }
            }, function()
            {
                self.alert('error get file from sdcard');
                reject();
            });
        }
        // file already present: go to next file
        else
        {
            if(files.length>0)
            {
                self.upload_dir_each(source,destination,api_files,files, total).then(ok,reject);
            }
            else
            {
                ok();
            }
        }
    });
};

Files.prototype.new_folder = function(id, name)
{
    var self=this;

    var newname = prompt(translate('enter_new_name'));
    if(newname)
    {
        this.api.mkdir(id, newname).then(function()
        {
            self.update();
        }, function()
        {
            self.alert(translate('error_mkdir'));
        });
    }
};


Files.prototype.previous = function(numpop)
{
    if(!numpop) { numpop=0; }

    if(this.subdirs.length>1)
    {
        var current;
        var prev;

        var current= this.subdirs.pop();
        var prev= this.subdirs.pop();
        while(numpop>0)
        {
            var prev= this.subdirs.pop();
            numpop--;
        }
        this.buildDir(prev.id, prev.name, false);
    }
};

Files.prototype.buildDir_db = function(id, name)
{
    var self=this;
    return new Promise(function(ok, reject)
    {
        var transaction = self.db.transaction([ 'files' ], 'readwrite');
        var files_db = transaction.objectStore('files');
        var files_index = files_db.index('parent');
        
        var file_found=false;
        var files=[];
        var dirs=[];
        files_index.get(id);
        var rangeTest = IDBKeyRange.only(id);
        files_index.openCursor(rangeTest).onsuccess = function(event)
        {
            var cursor = event.target.result;
            if (cursor) {
                file_found=true;
                if(cursor.value.is_folder)
                {
                    dirs.push(cursor.value);
                }
                else
                {
                    files.push(cursor.value);
                }
                cursor.continue();
            }
            else
            {
                if(file_found)
                {
                    self.buildDir_receive(id, name, {pageToken:null, items:Array.concat(dirs, files)});
                    ok();
                } 
                else
                {
                    reject();
                }
            }
        };
        files_index.onerror= reject;
    });
};

Files.prototype.clearCache = function(id)
{
    var self=this;
    return new Promise(function(ok, reject)
    {
        var transaction = self.db.transaction([ 'files' ], 'readwrite');
        var files_db = transaction.objectStore('files');
        var files_index = files_db.index('parent');
        
        files_index.get(id);
        var rangeTest = IDBKeyRange.only(id);
        files_index.openCursor(rangeTest).onsuccess = function(event)
        {
            var cursor = event.target.result;
            if (cursor) {
                try
                {
                    files_db.delete(cursor.value.parent_id);
                }
                catch(err) { }
                cursor.continue();
            }
            else
            {
                ok();
            }
        };
    });
};

Files.prototype.update_status= function()
{
    // Default state of upload btn is hidden
    if(this.upload_btn)
    {
        this.upload_btn.classList.add('hidden');
    }
    if(this.api.search)
    {
        this.search_btn.classList.remove('hidden');
    }
    else
    {
        this.search_btn.classList.add('hidden');
    }

    if(this.current_id===this.accountid+'_/')
    {
        this.home_btn.classList.add('disabled');
    }
    else
    {
        this.home_btn.classList.remove('disabled');
    }

    if(/special_/.test(this.current_id) || (this.current_id===this.accountid+'_/' && this.api.type=='Google'))
    {
        this.add_btn.classList.add('disabled');
    }

    else
    {
        this.add_btn.classList.remove('disabled');

        if(CustomActivity.isSharing && this.upload_btn)
        {
            this.upload_btn.classList.remove('hidden');
        }
    }

    this.subdirs.push({ id: this.current_id, name: this.current_name});

    // Loading file cursor
    var li = document.createElement('li');
    li.className='fa fa-refresh fa-spin fa-3x fa-fw loading';
    this.files_container.innerHTML='';
    this.files_container.appendChild(li);
};

Files.prototype.buildDir = function(id, name, force_api)
{
    var self=this;
    this.selected={};
    this.items={};
    this.current_id = id;
    this.current_name = name;

    this.update_status();

    // special case for search term
    if(id=='special_search')
    {
        return this.search(this.current_name);
    }

    else if(this.api.nocache===true)
    {
        if(force_api && this.api.type=='Internal')
        {
            sdcard.update().then(function()
            {
                self.buildDir_api(id,name);
            }, function()
            {
                self.alert(translate('error_loading_dir_structure'));
            });
        }
        else
        {
            self.buildDir_api(id,name);
        }
    }
    // force refresh from api, if needed
    else if(online.online && (force_api || (this.update_fetched!==null && !this.update_fetched[id])))
    {
        this.clearCache(id).then(this.buildDir_api.bind(this,id,name));
    }
    // load from db then api if not available
    else
    {
        this.buildDir_db(id, name)
            .catch(this.buildDir_api.bind(this, id,name));
    }

    // Root dir, no previous button
    if(self.subdirs.length==1)
    {
        self.files_ariane.innerHTML= '';
        text = document.createTextNode(' / ');
        self.files_ariane.appendChild(text);
    }
    else
    {
        self.files_ariane.innerHTML= '';
        span = document.createElement('div');
        span.className='fa fa-arrow-left ariane_previous';
        span.addEventListener('click', vibrate.button.bind(vibrate));
        span.addEventListener('click', self.previous.bind(self));
        self.files_ariane.appendChild(span);

        div = document.createElement('div');
        div.className='files_ariane_overflow';
        self.subdirs.reverse().forEach(function(dir,idx)
        {
            if(idx+1 != self.subdirs.length)
            {
                span = document.createElement('span');
                span.addEventListener('click', vibrate.button.bind(vibrate));
                span.addEventListener('click', self.previous.bind(self, idx-1));

                text = document.createTextNode(' / '+dir.name);
                span.appendChild(text);
                div.appendChild(span);
            }
            else if(dir.id=='special_search')
            {
                span = document.createElement('span');
                span.addEventListener('click', vibrate.button.bind(vibrate));
                span.addEventListener('click', self.previous.bind(self, idx-1));

                text = document.createTextNode(translate('menu_search')+' : '+dir.name);
                span.appendChild(text);
                div.appendChild(span);
            }
        });

        // back to subdirs order
        self.subdirs.reverse();
        self.files_ariane.appendChild(div);
    }
};

Files.prototype.buildDir_receive = function(id, name, data)
{
    var self=this;

    // Do not add items if loaded from another directory
    if(id!=this.current_id)
    {
        return;
    }

    this.files_container.innerHTML='';
    self.addItems(id,data);
};

Files.prototype.buildDir_api = function(id, name)
{
    var self=this;


    // if we need to know that this directory is up to date
    if(this.update_fetched!==null) {
        this.update_fetched[id]=1;
    }

    this.api.list_dir(id)
        .then(function(data)
        {
            self.buildDir_receive(id, name, data);

            // add to db
            var transaction = self.db.transaction([ 'files' ], 'readwrite');
            var files_db = transaction.objectStore('files');
            files_db.onsuccess= function()
            {
            };
            files_db.onerror= function(err)
            {
                //alert('error add db files'+err);
            };
            var items = data.items;

            if(self.api.nocache!==true)
            {
                items.forEach(function(item)
                {
                    item.parent_id = item.parent+'_'+item.id;
                    try
                    {
                        files_db.put(item);
                    }
                    catch(err)
                    {
                        console.error('error ', err, item);
                    }
                });
            }
        }, function()
        {
            console.error('fail receiving list_dir');
            self.buildDir_receive(id, name, null);
        });
};

Files.prototype.addItems = function(parent,data)
{
    var self=this;
    var items = data ? data.items : [];
    var show_thumbnails = settings.getShowThumbnails();

    var docfrag = document.createDocumentFragment();
    var first_file=false;

    // do not sort trash bin
    if(!/special_trashed/.test(parent))
    {
        items.sort(function(a,b)
        {
            if(a.is_folder != b.is_folder)
            {
                return b.is_folder - a.is_folder;
            }
            else
            {
                var aname = a.name.toUpperCase();
                var bname = b.name.toUpperCase();

                if(aname > bname) return 1;
                if(aname < bname) return -1;
                return 0;
            }
        });
    }
    if(items.length===0)
    {
        var li = document.createElement('li');
        li.className='no_items';
        li.innerHTML=data ? translate('no_files_found') : translate('error_listing_directory');
        docfrag.appendChild(li);
    }
    items.forEach(function(item)
    {
        var p;
        var p_thumb;
        var li = document.createElement('li');
        li.setAttribute('data-id', item.id);
        li.className=item.is_folder?'folder':'file';
        self.items[item.id]= item;
        if(item.special)
        {
            li.classList.add('special');
        }

        var div=document.createElement('div');
        div.className='item_container';
        li.appendChild(div);

        if(!item.is_folder)
        {
            if(first_file)
            {
                var separator= document.createElement('li');
                separator.className='separator';
                docfrag.appendChild(separator);
                first_file=false;
            }
            p_thumb = document.createElement('p');
            p_thumb.className='item_thumb';
            div.appendChild(p_thumb);

            if(item.thumbnail && show_thumbnails)
            {
                li.classList.add('thumb_loading');
                p_thumb.classList.add('loading');

            }
            else
            {
                p_thumb.classList.add('item_thumb_small');
            }
            div.addEventListener('click',  vibrate.button.bind(vibrate));
            div.addEventListener('click',  self.edit.bind(self,item));
        }
        else
        {
            first_file=true;
            if(item.is_trashed)
            {
                div.addEventListener('click',  vibrate.button.bind(vibrate));
                div.addEventListener('click',  self.edit.bind(self,item));
            }
            else
            {
                div.addEventListener('click', vibrate.button.bind(vibrate));
                div.addEventListener('click', self.buildDir.bind(self, item.id , item.name, false));
            }
        }

        var fileinfo = document.createElement('div');
        fileinfo.className='file_info';
        div.appendChild(fileinfo);

        var p_name = document.createElement('p');
        p_name.className='item_name';

        sdcard.cache_image(item.icon.replace(/[^\w\.]+/g,''), item.icon, p_name)
        .then((function(p,content)
        {
            if(p_thumb && (!item.thumbnail || !show_thumbnails))
            {
                p_thumb.style.backgroundImage='url('+(content)+')';
            }
            p_name.style.backgroundImage='url('+(content)+')';
        }).bind(this,p_name));

        var txt = document.createTextNode(item.name);
        p_name.appendChild(txt);
        fileinfo.appendChild(p_name);


        if(!item.special)
        {
            if(item.is_folder)
            {
                p = document.createElement('p');
                p.className='item_info fa fa-info-circle';
                p.addEventListener('click', vibrate.button.bind(vibrate));
                p.addEventListener('click', self.edit.bind(self, item));
                fileinfo.appendChild(p);
            }
            else
            {
                p = document.createElement('p');
                p.className='item_check fa fa-square-o';
                fileinfo.appendChild(p);

                // On detail list, only the square icon is clickable.
                if(settings.getShowDetails())
                {
                    p.addEventListener('click', vibrate.button.bind(vibrate));
                    p.addEventListener('click', self.select.bind(self, item));
                }
                else
                {
                    fileinfo.addEventListener('click', vibrate.button.bind(vibrate));
                    fileinfo.addEventListener('click', self.select.bind(self, item));
                }
            }
            if(item.is_starred)
            {
                li.classList.add('starred');
                p = document.createElement('p');
                p.className='item_starred fa fa-star';
                fileinfo.appendChild(p);
            }
        }
        if(!item.is_folder)
        {
            p = document.createElement('p');
            p.className='item_size';
            p.innerHTML=self.getReadableFileSizeString(item.size);
            fileinfo.appendChild(p);
        }


        docfrag.appendChild(li);
    });
    this.files_container.appendChild(docfrag);

    if(show_thumbnails)
    {
        // Load 3 images in parallel
        this.load_images();
        if(this.api.type!='Internal')
        {
            this.load_images();
            this.load_images();
        }
    }
};

Files.prototype.load_images = function(e)
{
    var self=this;
    var top = this.files_container.scrollTop;
    var li = document.querySelector('.thumb_loading');

    if(li)
    {
        var p = li.querySelector('.item_thumb.loading');
        li.classList.remove('thumb_loading');
        var item = this.items[li.getAttribute('data-id')] ;

        sdcard.cache_image(item.realid, item.thumbnail, p)
            .then(function(content)
            {
                p.style.backgroundImage='url('+(content)+')';
                p.classList.remove('loading');
                self.load_images(); // Recursive call
            }, function()
            {
                p.classList.remove('loading');
                p.classList.add('error');
                self.load_images(); // Recursive call
            });
    }
};


Files.prototype.select = function(item,e)
{
    if(e)
    {
        if(e.preventDefault) { e.preventDefault(); }
        if(e.stopPropagation) { e.stopPropagation(); }
    }
    var item_container = e.target;
    while(item_container && !item_container.classList.contains('item_container'))
    {
        item_container = item_container.parentNode;
    }
    var check = item_container.querySelector('.item_check');

    // if not selected
    if(check.classList.contains('fa-square-o'))
    {
        check.classList.remove('fa-square-o');
        check.classList.add('fa-check-square-o');
        if(!this.selected[item.id])
        {
            this.selected[item.id] = item;
        }
    }
    // If selected, remove
    else
    {
        check.classList.remove('fa-check-square-o');
        check.classList.add('fa-square-o');
        delete this.selected[item.id];
    }
};


Files.prototype.open = function()
{
    if(!this.api)
    {
        files.alert(translate('create_or_connect_first'));
    }
    settings.close();
    this.files.classList.add('focused');
};

Files.prototype.close = function()
{
    this.files.classList.remove('focused');
};

Files.prototype.getReadableFileSizeString = function(fileSizeInBytes)
{
    var i = -1;

    var byteUnits = [' kB', ' MB', ' GB', ' TB', 'PB', 'EB', 'ZB', 'YB'];
    do {
        fileSizeInBytes = fileSizeInBytes / 1024;
        i++;
    } while (fileSizeInBytes > 1024);

    return Math.max(fileSizeInBytes, 0.1).toFixed(1) + byteUnits[i];
};

Files.prototype.alert = function(msg,time, extra)
{
    var self=this;

    if(!time) { time= 4000; }
    if(!Array.isArray(msg))
    {
        msg=[msg];
    }

    this.alert_msg.innerHTML='';
    msg.forEach(function(submsg)
    {
        var p = document.createElement('p');
        p.className='alert_main';

        var txt = document.createTextNode(submsg);
        p.appendChild(txt);
        self.alert_msg.appendChild(p);
    });

    if(extra)
    {
        p = document.createElement('p');
        p.className='alert_sub';
        txt = document.createTextNode(extra);
        p.appendChild(txt);
        this.alert_msg.appendChild(p);
    }

    this.alert_container.classList.add('visible');
    clearTimeout(this.alert_timeout);
    this.alert_timeout = setTimeout(this.alert_hide.bind(this), time);
};
Files.prototype.alert_hide = function()
{
    this.alert_container.classList.remove('visible');
};

Files.prototype.online = function()
{
    this.alert(translate('online_mode'));
    if(this.api && this.api.refresh_token)
    {
        this.api.refresh_token();
    }
};
Files.prototype.selectall = function()
{
    Array.forEach(this.files.querySelectorAll('.item_check.fa-square-o'), function(item)
    {
        item.click();
    });
    this.more();
};

Files.prototype.unselectall = function()
{
    Array.forEach(this.files.querySelectorAll('.item_check.fa-check-square-o'), function(item)
    {
        item.click();
    });
    this.more();
};



Files.prototype.offline = function()
{
    this.alert(translate('offline_mode'));
};

Files.prototype.no_provider = function(file, alternativelink)
{
    // choose to open as text if wanted to
    var select = new Selector();

    var div = document.createElement('div');
    var p = document.createElement('p');
    div.appendChild(p);
    p.innerHTML=translate('no_provider');

    var items = 
    [
        {
            'text':  translate('open_as_text'),
            'icon':  'file-text-o',
            'callback':  this.open_text.bind(self,file)
        }
    ];
    if(alternativelink)
    {
        items.push({
                'text':  translate('open_online'),
                'icon':  'arrow-right',
                'callback':  window.open.bind(window,alternativelink)
        });
    }

    select.create('',(file.name||'').replace(/.*\//,''), div, items);
};

Files.prototype.open_text = function(file)
{
    var reader = new window.FileReader();
    reader.readAsBinaryString(file); 
    reader.onloadend = function() {
        var selector = new Selector();
        var items=[];

        var textNode = document.createTextNode(reader.result);
        selector.create(
                '',
                file.name,
                textNode,
                items);
    };
};
