var Accounts = function()
{
    this.apis=[];
    this.accounts=[];
};

Accounts.prototype.bind = function()
{
    this.accounts_container = document.querySelector('.accounts ul');
};

Accounts.prototype.init = function()
{
    var self=this;
    this.bind();
    document.querySelector('.new_account').addEventListener('click', vibrate.button.bind(vibrate));
    document.querySelector('.new_account').addEventListener('click', this.create.bind(this));

    Array.forEach(document.querySelectorAll('.goto_accounts'), function(item)
    {
        item.addEventListener('click', vibrate.button.bind(vibrate));
        item.addEventListener('click', self.open.bind(self));
    });
};

Accounts.prototype.open = function()
{
    var self=this;

    settings.close();
    files.close();
    for(var id in this.accounts)
    {
        data = self.accounts[id];
        if(data.obj.getInfo)
        {
            self.updateInfo(data.obj,data.account);
        }
    }
};


Accounts.prototype.register = function(name, icon, obj)
{
    var found=false;
    this.apis.forEach(function(api)
    {
        if(api.name===name)
        {
            found=true;
        }
    });
    if(!found)
    {
        this.apis.push({ name: name, icon:icon, object: obj});
    }
};

Accounts.prototype.updateInfo = function(api,account)
{
    var id = 'account_info_'+(account.id+'').replace(/[^a-zA-Z0-9]/g,'');
    var p =document.getElementById(id);
    if(api.getInfo)
    {
        api.getInfo(account).then(function(data)
        {
            p.innerHTML  = data;
        }, function()
        {
            p.innerHTML  = translate('info_not_available');
        });
    }
    else
    {
        p.innerHTML  = translate('info_not_available');
    }
};

Accounts.prototype.add = function(account, obj)
{
    this.accounts['idx_'+account.id] = { account: account, obj: obj};

    var li = document.createElement('li');
    li.addEventListener('click', vibrate.button.bind(vibrate));
    li.addEventListener('click', this.select.bind(this, account.id, obj));

    li.setAttribute('data-id',account.id);
    li.setAttribute('data-id',account.id);
    li.classList.add('account');
    li.classList.add('account-'+account.type);

    var p =document.createElement('p');
    p.classList.add('account_name');
    p.innerHTML=account.email;
    li.appendChild(p);

    p =document.createElement('p');
    p.classList.add('account_data');
    p.id= 'account_info_'+(account.id+'').replace(/[^a-zA-Z0-9]/g,'');
    p.innerHTML= obj.getInfo ? translate('loading') : translate('info_not_available');
    li.appendChild(p);

    if(obj.delete_available!==false)
    {
        var p_delete =document.createElement('p');
        p_delete.className='account_delete fa fa-trash';
        p_delete.addEventListener('click', vibrate.button.bind(vibrate));
        p_delete.addEventListener('click', this.deleteAccount.bind(this, obj));
        li.appendChild(p_delete);
    }

    this.accounts_container.appendChild(li);

    if(obj.getInfo)
    {
        this.updateInfo(obj,account);
    }
};

Accounts.prototype.deleteAccount = function(obj,e)
{
    if(e.preventDefault) { e.preventDefault(); }
    if(e.stopPropagation) { e.stopPropagation(); }

    if(!confirm(translate('confirm_delete')))
    {
        return false;
    }
    var li =e.target;

    while(li && !li.classList.contains('account'))
    {
        li = li.parentNode;
    }

    obj.deleteAccount(li.getAttribute('data-id'))
        .then(function()
        {
            li.parentNode.removeChild(li);
        }, function(e)
        {
            files.alert(translate('error_deleting_account'));
        });
    delete this.accounts['idx_'+li.getAttribute('data-id')];
    return false;
};

Accounts.prototype.create = function()
{
    var selector = new Selector();
    var items=[];
    for(var i=0; i<this.apis.length; i++)
    {
        if(this.apis[i].object.create_account)
        {
            items.push({
                'text':  '<span class="api_list account-'+this.apis[i].icon+'"></span> '+this.apis[i].name,
                'icon':  null,
                'callback':  this.apis[i].object.create_account.bind(this.apis[i].object),
            });
        }
    }
    selector.create(
            null,
            translate('account_creation'),
            translate('select_account_type'),
            items);
};

Accounts.prototype.select = function(id,  obj)
{
    obj.setAccount(id)
    .then(files.setAccount.bind(files,id, obj));
};
