var Queue = function()
{
    var self=this;
    this.done = [];
    this.idx=0;
};

Queue.prototype.bind=function()
{
    this.goto_queue = document.querySelector('.goto_queue');
    this.goto_queue.addEventListener('click', this.open.bind(this));
    this.clear_next();
};

Queue.prototype.open=function()
{
    var self=this;
    if(this.open_selector && !this.open_selector.closed)
    {
        this.open_selector.close();
    }
    this.open_selector = new Selector();

    var div = document.createElement('div');
    var p;


    var clearall = document.createElement('p');
    clearall.className='clear_next';
    var b = document.createElement('button');
    b.className='bb-button bb-delete';
    b.innerHTML=translate('queue_clear_next');
    b.addEventListener('click', function()
    {
        if((self.queue.length>0 || self.current) && confirm(translate('queue_clear_confirm')))
        {
            self.clear_next();
            self.open_selector.close();
            self.open();
        }
    });
    clearall.appendChild(b);
    div.appendChild(clearall);


    // CURRENT ITEM
    p = document.createElement('p');
    p.className='queue_section';
    p.innerHTML=translate('queue_current_item');
    div.appendChild(p);

    p = document.createElement('p');
    p.className='queue_current';
    if(this.current)
    {
        p.innerHTML  = '<span class="fa fa-'+this.current.action+'"></span> '+this.current.text;
    }
    else
    {
        p.innerHTML  = '-';
    }
    div.appendChild(p);

    p = document.createElement('p');
    p.className='queue_section';
    p.innerHTML=translate('queue_next_items')+' (<span class="queue_next_num">'+this.queue.length+'</span>)';
    div.appendChild(p);

    ul = document.createElement('ul');
    ul.className='queue_next';
    if(this.queue.length)
    {
        this.queue.forEach(function(item)
        {
            li = document.createElement('li');
            li.id='queue_'+item.id;
            li.innerHTML  = '<span class="fa fa-'+item.action+'"></span> '+item.text;
            ul.appendChild(li);
        });
    }
    div.appendChild(ul);


    p = document.createElement('p');
    p.innerHTML=translate('queue_done_items');
    p.className='queue_section';
    div.appendChild(p);

    ul = document.createElement('ul');
    ul.className='queue_done';
    if(this.done.length)
    {
        this.done.forEach(function(item)
        {
            li = document.createElement('li');
            li.className=item.status ? 'queue_ok' :'queue_ko';
            li.id='queue_'+item.id;
            li.innerHTML  = '<span class="fa fa-'+item.action+'"></span> '+item.text;
            ul.appendChild(li);
        });
    }
    div.appendChild(ul);

    this.open_selector.create(
            'queue_selector',
            translate('queue_title'),
            div,
            []);
};

Queue.prototype.update=function()
{
    if(this.open_selector && !this.open_selector.closed)
    {
        document.querySelector('.queue_next_num').innerHTML = this.queue.length;
    }
};

Queue.prototype.add_current=function(item)
{
    this.current=item;
    if(this.open_selector && !this.open_selector.closed)
    {
        document.querySelector('.queue_current').innerHTML = item ?  '<span class="fa fa-'+item.action+'"></span> '+item.text : '-';
        if(item)
        {
            document.querySelector('#queue_'+item.id).remove();
            document.querySelector('.queue_current').innerHTML = '<span class="fa fa-'+item.action+'"></span> '+item.text;
            this.update();
        }
        else
        {
            document.querySelector('.queue_current').innerHTML = '-';
            this.update();
        }
    }
};

Queue.prototype.add=function(api,text,data, callback)
{
    files.alert(translate('queue_added'));
    this.idx++;

    this.queue.push({
            id: this.idx,
            action: data[0]=='create'  ? 'upload' : 'download',
            api_type: api.type,
            text: text,
            api_account: api.account,
            data: data,
            callback: callback
    });
    if(!this.running)
    {
        this.run();
    }
}

Queue.prototype.add_done=function(item, status)
{
    this.done.unshift({ action:item.action, text: item.text, status: status});
    if(!status)
    {
        this.error=true;
    }

    // Limit done items to last 100 items
    this.done = this.done.slice(-50);

    if(this.open_selector && !this.open_selector.closed)
    {
        document.querySelector('.queue_current').innerHTML = '-';

        var li = document.createElement('li');
        li.className= status ? 'queue_ok' :'queue_ko';
        li.id='queue_'+item.id;
        li.innerHTML  = '<span class="fa fa-'+item.action+'"></span> '+item.text;

        var done = document.querySelector('.queue_done')
        done.insertBefore(li, done.childNodes[0]);

        this.update();
    }
};


Queue.prototype.run = function(item)
{
    var self=this;
    var item = this.queue.shift();
    this.goto_queue.classList.add('active');

    this.add_current(item);

    if(item)
    {
        this.running=true;
        /*
        files.alert(
            [
                translate('queue_running'),
                translate('queue_left',{'left': this.queue.length})
            ], null,item.text);
        */

        var api = new window[item.api_type]();
        var action = item.data.shift();
        api.initDb().then(function()
        {
            api.account = item.api_account;

            api.setAccount(item.api_account.id)
                .then(function() {
                    api[action].apply(api,item.data).then(function()
                    {
                        if(api.deco)
                        {
                            api.deco();
                        }
                        //files.alert(translate('queue_done'), null,item.text);

                        self.add_done(item, true);
                        if(item.callback)
                        {
                            item.callback();
                        }

                        if(action=='create')
                        {
                            accounts.updateInfo(internal, internal.account);
                        }
                        else
                        {
                            accounts.updateInfo(api, item.api_account);
                        }


                        self.run();
                    }, function()
                    {
                        console.error('error action queue ', action, item.action);
                        files.alert(translate('queue_error'), null,item.text);
                        self.add_done(item, false);
                        self.run();
                    });
                }, function()
                {
                    files.alert(translate('queue_error'), null,item.text);
                    console.error('error set account queue');
                    self.add_done(item, false);
                    self.run();
                })
        }, function()
        {
            console.error('error init db queue');
            files.alert(translate('queue_error'), null,item.text);
            self.add_done(item, false);
            self.run();
        })
    }
    else
    {
        files.update();
        sdcard.update();

        // if it was running
        if(this.running)
        {
            if(this.error)
            {
                this.error=false;
                files.alert(translate('queue_all_done_with_errors'));
            }
            else
            {
                files.alert(translate('queue_all_done'));
            }
        }

        this.running=false;
        this.goto_queue.classList.remove('active');
    }
};

Queue.prototype.clear_next = function()
{
    this.current=null;
    this.running=false;
    this.queue=[];
    this.error = false;
};



Queue.prototype.init = function()
{
    this.bind();
};

